echo "Testing endpoint is returning a 200..."

aceservername=is-toolkit

url=$(oc get routes -o custom-columns=ROUTE:.spec.host -l release=$aceservername,app.kubernetes.io/component=http --no-headers)

echo "URL is: $url"

if [[ "$(curl -o /dev/null --silent --head --write-out "%{http_code}" $url)" != "200" ]]
then
    printf "Server is not serving json properly"
    exit 1
else
    printf "Server is serving json properly.\n\n"
fi